# Template Fields

This repository contains the definition of the database structure of OneFleet and SARchive.

The data definition is deployed to [Data Docs created by Great Expectations](https://civilmrcc.gitlab.io/template-fields/index.html)

## Install Locally
### Prerequisites
You need to have Python installed on your system to run [Great Expectations](https://greatexpectations.io/)

### Local Setup

```bash
python3 -m venv .venv
source .venv/bin/activate
pip3 install --upgrade pip setuptools wheel
pip install -r requirements.txt
```

## Publish Changes

To publish your changes to the [Data Docs created by Great Expectations](https://civilmrcc.gitlab.io/template-fields/index.html). Make sure, to run 

```bash
make publish
```

before committing your changes to `main`. 