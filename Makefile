publish:
	# Make sure you enabled the virtual environment of the project
	great_expectations --v3-api -y docs build -sn gitlab_site -nv

publish-local:
	# Make sure you enabled the virtual environment of the project
	great_expectations --v3-api -y docs build -sn local_site
